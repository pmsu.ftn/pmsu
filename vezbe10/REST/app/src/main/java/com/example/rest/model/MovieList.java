package com.example.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MovieList {
    @SerializedName("page")
    private String page;
    @SerializedName("results")
    private List<Movie> movies;

    public MovieList(String page, List<Movie> movies) {
        this.page = page;
        this.movies = movies;
    }

    public String getPage() {
        return page;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }
}
