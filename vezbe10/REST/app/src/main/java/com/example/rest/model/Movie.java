package com.example.rest.model;

import com.google.gson.annotations.SerializedName;

public class Movie {
    @SerializedName("id")
    private Integer id;
    @SerializedName("vote_average")
    private Float voteAverage;
    @SerializedName("poster_path")
    private String posterPath;
    @SerializedName("title")
    private String title;
    @SerializedName("release_date")
    private String releaseDate;
    @SerializedName("overview")
    private String overview;
    @SerializedName("backdrop_path")
    private String image_path;

    public Movie(Integer id, Float voteAverage, String posterPath, String title, String releaseDate, String overview, String image_path) {
        this.id = id;
        this.voteAverage = voteAverage;
        this.posterPath = posterPath;
        this.title = title;
        this.releaseDate = releaseDate;
        this.overview = overview;
        this.image_path = image_path;
    }

    public Float getVoteAverage() {
        return voteAverage;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public String getTitle() {
        return title;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public String getOverview() {
        return overview;
    }

    public Integer getId() {
        return id;
    }

    public String getImage_path() {
        return image_path;
    }
}
