package com.example.rest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rest.adapter.MovieListAdapter;
import com.example.rest.model.Movie;
import com.example.rest.model.MovieList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MovieActivity extends AppCompatActivity {
    int id;
    static final String TAG = MainActivity.class.getSimpleName();
    static final String BASE_URL = "https://api.themoviedb.org/3/";
    static final String BASE_URL_IMAGES = "https://image.tmdb.org/t/p/w185/";
    static Retrofit retrofit = null;
    static Retrofit retrofitImage = null;
    final static String API_KEY = "95a1636bd3ddaa4b668130b0a8025920";
    private RecyclerView recyclerView;
    private String image_path = "";

    private ImageView image;
    private TextView title;
    private TextView releaseDate;
    TextView vote;
    TextView overview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);

        Bundle b = getIntent().getExtras();
        if(b != null)
            id = b.getInt("ID");

        image = findViewById(R.id.ivMovie);
        title = findViewById(R.id.tvTitle);
        releaseDate =  findViewById(R.id.tvReleaseDate);
        vote = findViewById(R.id.tvVote);
        overview = findViewById(R.id.tvOverview);
    }

    @Override
    public void onResume(){
        super.onResume();
        getSelecetedMovie();
    }

    private void getSelecetedMovie(){
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        if (retrofitImage == null) {
            retrofitImage = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(BASE_URL_IMAGES)
                    .build();
        }

        MovieApiService movieApiService = retrofit.create(MovieApiService.class);
        MovieApiService movieApiServiceImage = retrofitImage.create(MovieApiService.class);
        Call<Movie> call = movieApiService.getMovie(id, API_KEY);



        call.enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
                title.setText(response.body().getTitle());
                releaseDate.setText(response.body().getReleaseDate());
                vote.setText(response.body().getVoteAverage().toString());
                overview.setText(response.body().getOverview());
                image_path = response.body().getImage_path().substring(1);

                Call<ResponseBody> callImage = movieApiServiceImage.getImage(image_path, API_KEY);
                callImage.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Bitmap bmp = BitmapFactory.decodeStream(response.body().byteStream());
                        image.setImageBitmap(bmp);
                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                        Log.e(TAG, throwable.toString());
                    }
                });
            }
            @Override
            public void onFailure(Call<Movie> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });




    }
}