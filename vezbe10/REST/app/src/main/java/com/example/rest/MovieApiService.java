package com.example.rest;

import com.example.rest.model.Movie;
import com.example.rest.model.MovieList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface MovieApiService {
    @GET("movie/{id}")
    Call<Movie> getMovie(@Path("id") int id, @Query("api_key") String apiKey);

    @GET("movie/top_rated")
    Call<MovieList> getTopRatedMovies(@Query("api_key") String apiKey);

    @GET("{image_path}")
    Call<ResponseBody> getImage(@Path("image_path") String image_path, @Query("api_key") String apiKey);
}
