package com.example.sqlite;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.sqlite.adapter.CustomAdapter;
import com.example.sqlite.model.Contact;
import com.example.sqlite.sql.DatabaseHandler;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    List<Contact> dataModels;
    ListView listView;
    private static CustomAdapter adapter;
    DatabaseHandler dbh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbh = new DatabaseHandler(this);

        listView=(ListView)findViewById(R.id.list);

        dbh.clearTable();

        dbh.addContact(new Contact(1, "Petar", "065749234"));
        dbh.addContact(new Contact(2, "Jovan", "065749234"));
        dbh.addContact(new Contact(3, "Mitar", "065749234"));
        dbh.addContact(new Contact(4, "Relja", "065749234"));
        dbh.addContact(new Contact(5, "Mile", "065749234"));
        dbh.addContact(new Contact(6, "Aleksandar", "065749234"));
        dbh.addContact(new Contact(7, "Luka", "065749234"));
        dbh.addContact(new Contact(8, "Ivan", "065749234"));



        dataModels= dbh.getAllContacts();

        adapter= new CustomAdapter(new ArrayList<Contact>(dataModels),getApplicationContext());

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Contact dataModel= dataModels.get(position);

                Snackbar.make(view, dataModel.getID()+"  "+dataModel.getName()+"  "+dataModel.getPhoneNumber(), Snackbar.LENGTH_LONG)
                        .setAction("No action", null).show();
            }
        });
    }

}