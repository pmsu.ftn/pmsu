package com.example.sqlite.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.sqlite.R;
import com.example.sqlite.model.Contact;

import java.util.ArrayList;

public class CustomAdapter extends ArrayAdapter<Contact> implements View.OnClickListener{

    private ArrayList<Contact> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView id;
        TextView name;
        TextView phone;
    }

    public CustomAdapter(ArrayList<Contact> data, Context context) {
        super(context, R.layout.row_item, data);
        this.dataSet = data;
        this.mContext=context;

    }

    @Override
    public void onClick(View v) {

    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Contact dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_item, parent, false);
            viewHolder.id = (TextView) convertView.findViewById(R.id.identifier);
            viewHolder.name = (TextView) convertView.findViewById(R.id.name);
            viewHolder.phone = (TextView) convertView.findViewById(R.id.phone_number);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.id.setText(dataModel.getName());
        viewHolder.name.setText(dataModel.getName());
        viewHolder.phone.setText(dataModel.getPhoneNumber());
        // Return the completed view to render on screen
        return convertView;
    }
}
