package com.example.preferencesandfiles;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnPreferences = findViewById(R.id.btnPreferences);
        Button btnFiles = findViewById(R.id.btnFiles);

        btnPreferences.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent activityChangeIntent = new Intent(getApplicationContext(), Preferences.class);
                startActivity(activityChangeIntent);
            }
        });

        btnFiles.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent activityChangeIntent = new Intent(getApplicationContext(), Files.class);
                startActivity(activityChangeIntent);
            }
        });

    }

}